CS 1440 - Fri Dec 08

Get on the FSLC mailing list:
http://tinyurl.com/usufslc

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

Final Exam Remarks
==================

The testing center has reached capacity this semester, meaning that you may
not be able to schedule your appointment. There is an overflow room and
walk-ins are encouraged.

If you haven't been able to schedule a time slot, you will have to take the
test as a walk-in. They will get you seated ASAP so that you can take your
exam.


-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 _   ___        _______   ___                         
| | | \ \      / /___  | |_ _|___ ___ _   _  ___  ___ 
| |_| |\ \ /\ / /   / /   | |/ __/ __| | | |/ _ \/ __|
|  _  | \ V  V /   / /    | |\__ \__ \ |_| |  __/\__ \
|_| |_|  \_/\_/   /_/    |___|___/___/\__,_|\___||___/

Q. How long should my program run?
A. My program crawls http://cs.usu.edu to a depth of 3 links in ~5 sec

Q. Is it normal for Valgrind to find memory leaks?
A. Read the stack traces for the leaks Valgrind finds. My implementation
   reports ~100kb possibly lost, but it's all library code which I didn't
   write. Also, fixing memory leaks isn't a priority on the rubric, so don't
   feel obligated to spend time on it.
                                                      
Q. How many links per page are there, and what happens if I miss some?
A. Per yesterday's announcement, your crawler doesn't have to be 100%
   accurate. It's okay if it misses some URLs here and there. Strive instead
   for a regex that
       0) doesn't recognize strings which are *NOT* URLs
          ex. if your program treats something like this as a URL:
            "<div class="href">", that's a bad thing.
            Other examples:
            <img src="http://images.google.com/arstartarst.png">
            <link type="stylesheet" href="http://style.google.com/tarst.css">
       1) doesn't cause a segfault

Q. Should my output look like your sample output?
A. Yes and no.
   0) Yes: your output *should* indicate recursion depth. Indentation is what
      I did, and yields pleasing results
   1) No: what your output looks like depends upon which URLs you visit, which
      in turn depends upon 
          1.0) the accuracy of your regex and
          1.1) what those websites contained at the moment your crawler read them
      Per the previous question, 1.0 will vary from student to student and
      1.1 isn't under our control.

Q. How do I use a std::map?
A. It's like an array which can use a string as a subscript instead of an
   integer.

   For example, an array might be used like this:  a[0] = a[1];
   A map might be used like so:                    m["apples"] = m["oranges"];
   Please refer to examples/stdMapExample.cpp for a more in-depth explanation.

Q. What is the std::map for?
A. To make your program run faster by remembering which URLs you've already
   visited so you can avoid re-visiting them.

   The idea we're going for is that you can use a URL as an index into the
   map; the presence of that key tells you that your crawler has seen that URL
   before and can avoid re-visiting it. The value you store in the map at that
   index doesn't matter for our program; all we care about is which keys do or
   don't exist.

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 _ _                                                                          
( | ) Just remember that your programming education is your                   
 V V  responsibility, even when you take a class. A course will provide a     
      framework for acquiring a grade and credit at the end of the term,      
      but that framework doesn't limit you in your learning. Think of         
      your time in the class as a great opportunity to learn as much       _ _ 
      about the subject as possible, beyond any objectives listed in the  ( | )
      course syllabus.                                                     V V 

                                                              -- V. Anton Spraul

  ___ _              _             ___             _    
 / __| |_  __ _ _ __| |_ ___ _ _  ( _ )  __ _ _  _(_)___
| (__| ' \/ _` | '_ \  _/ -_) '_| / _ \ / _` | || | |_ /
 \___|_||_\__,_| .__/\__\___|_|   \___/ \__, |\_,_|_/__|
               |_|                         |_|          

Defeating coding errors
    if (0 == i)
         vs.
    if (0 = i)

Defeating design errors
    (Don't write programs like this)
    https://github.com/EnterpriseQualityCoding/FizzBuzzEnterpriseEdition

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 ___          _   _                     _     _        _ 
| _ \__ _ _ _| |_(_)_ _  __ _   __ _ __| |_ _(_)__ ___(_)
|  _/ _` | '_|  _| | ' \/ _` | / _` / _` \ V / / _/ -_)_ 
|_| \__,_|_|  \__|_|_||_\__, | \__,_\__,_|\_/|_\__\___(_)
                        |___/                            

    Learn your personal strengths/weaknesses
    ----------------------------------------


    Learn many programming languages
    --------------------------------
 _ _                                _ _ 
( | ) The more languages you know, ( | )
 V V  the more places you'll go.    V V 

               -- Dr. Seuss (allegedly)

    I find that having a specific project in mind is helpful to avoid getting
    stuck. But before you're ready to take on a project in a new language, it
    helps to think small. When you're first starting out in a new language
    small programs are useful to explore the basics.

        https://programmingpraxis.com/
        https://projecteuler.net/
        http://adventofcode.com/

    Another way I've studied a new language is to take a program I've already
    written in one language and translated it into the new language. This
    really highlights the essential/accidental aspects of the original
    program, and contrasts the two languages. From this I can form an opinion
    about which language is better.

NOT necessarily a repository of admirable code...:
    https://github.com/fadein
