CS1440 - Fall 2017                                Name:
Think Like a Programmer
Chapter 7 Quiz                                A Number:




-[ Question #1 ]-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-

Match the following units of reuse with their description

Abstract Data Type  Code that has been copied-and-pasted from one
                    project to another
                    
Patterns            A programming recipe for solving a specific problem

                    
Code Block          A template for a particular programming situation

                    
Libraries           This is defined by which operations are available, not by
                    its implementation
                    
Algorithms          An organized collection of related pieces of code





-[ Question #2 ]-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-

Number these units of reuse in order from       __________ Abstract Data Type
0 (least flexible) to                       
3 (most flexible):                              __________ Library
                                            
                                                __________ Algorithm
                                            
                                                __________ Pattern
                                           




-[ Question #3 ]-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-

The strategy (a.k.a. policy) pattern is used when

 [*]  your program is able to choose at runtime which algorithm to use

 [ ]  your program enforces a policy of strictly separating user and
      kernel data

 [ ]  your program follows a strategy such as divide-and-conquer 

 [ ]  your development process deliberately follows a software component reuse
      strategy





-[ Question #4 ]-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-

True / False: Knowing when (and when not) to use a technique is as important as
              knowing how to use it.




-[ Question #5 ]-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-~-=-

True / False: Much "real world" programming involves supplementing or modifying
              an existing code base.
