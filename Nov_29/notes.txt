CS 2610 - Wed Nov 29

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 ___ ___  ___   _     ___
|_ _|   \| __| /_\   / __|_  _ _ ___ _____ _  _ ___
 | || |) | _| / _ \  \__ \ || | '_\ V / -_) || (_-<
|___|___/|___/_/ \_\ |___/\_,_|_|  \_/\___|\_, /__/
                                           |__/
The IDEA survey is open until Dec 10


-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-


HOWTO break your C9 workspace on the Quickfast:

    $ sudo apt upgrade
        This will use up your allotted disk space and leave your workspace in
        a broken state

    Mine Bitcoins on your workspace
        They know o_O


-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-


Last time we talked about "Sharpening the Saw"; this entails many things:

    * taking note of your performance; noticing when you're not being effective

    * investing sufficient time in keeping your skills fresh and up-to-date

    * deliberatly practicing and learning new things, even if it isn't directly
      relevant to your present project

I focused on the practical side of "Sharpening the Saw", but today I want to
encourage you to sharpen your "theoretical" saw, too:

Turing's Curse
    https://www.youtube.com/embed/hVZxkFAIziA?autoplay=0&rel=0

tl;dw - those who don't understand technologies of the past are doomed to
re-implement them.

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 _ _
( | ) Some people, when confronted with a problem,   _ _
 V V  think "I know, I'll use regular expressions." ( | )
      Now they have two problems.                    V V

      -- JWZ


I actually brought that video up because I wanted to talk about a concept many
people find "boring" and "theoretical", but is actually very interesting not
only because it brings together three disciplines, but also because it happens
to have a very practical application, and its origin story is really neat.


Regular Expressions
===================

	https://en.wikipedia.org/wiki/Regular_expression

	A regular expression (regex or regexp) is, in theoretical computer science
	and formal language theory, a sequence of characters that define a search
	pattern.  Usually this pattern is then used by string searching algorithms
	for "find" or "find and replace" operations on strings.

	Regular expressions are used in search engines, search and replace dialogs
	of word processors and text editors, in text processing utilities such as
	sed and AWK and in lexical analysis.

    The concept arose in the 1950s when the American mathematician Stephen
    Kleene (KLAY-nee) formalized the description of a regular language. Kleene
    also created a branch of mathematical logic known as "recursion theory", so
    his work should hold a special place in your heart.

    The concept of regular expressions came into common use with Unix
    text-processing utilities. Since the 1980s, different syntaxes for writing
    regular expressions exist, one being the POSIX standard and another, widely
    used, being the Perl syntax. C++ follows the ECMAScript/JavaScript syntax
    for its regular expressions.


What using Regexes is like
==========================

    Many programming languages provide regex capabilities, built-in, or via
    libraries. In C++ regular expressions are part of the Standard Template
    Library and are brought into your program with

    #include <regex>

    Regular expressions are a language unto themselves, meaning that they have
    their own peculiar syntax. We write them in our C++ source code as string
    literals - we're literally writing programs using another programming
    language within our programming language! Their syntax happens to involve
    lots of backslashes. But backslashes already serve a role by introducing an
    escape sequence within string literals in C++, so we'll need to escape our
    escape character by doubling up our backslashes!


What sort of language is Regex?
===============================

	In CS lingo, a regular expression describes a state machine called a
	"deterministic finite automaton" (DFA). In other words, we're writing a
	compact program which describes a computer that will match a pattern, one
	byte at a time.


	Cool story:
	-----------
	Ken Thompson is an interesting hacker that you should read about. His
	Turing Award acceptance speech (the ACM Turing Award is the Nobel Prize of
	the CS world) about why you can never trust your computer is the most
	memorable and influential of them all.

	He is responsivle for introducing regular expressions into a text editor
	called QED, which went on to influence ed, sed, ex, vi, vim, etc., and by
	so doing made them widespread and common to all programmers.

	The clever thing he did with QED was to compile regular expressions,
	on-the-fly, into machine code implementations of the equivalent NDFA
	"non-deterministic finite automaton", which was then converted into a DFA
	and then executed.

	TL;DR Ken Thompson's text editor was also a compiler. He did this in the
	mid-sixties.

	https://www.bell-labs.com/usr/dmr/www/qed.html
	https://en.wikipedia.org/wiki/Thompson%27s_construction



	Basically, regexes are machines
	-------------------------------
	What can these machines do? Let's take them out for a spin and find out!  A
	program or website which lets you visualize your regex pattern is supremely
	helpful.

    There are many online resources that can help you build and debug regular
    expressions, including:

    https://regex101.com/ (select the javascript flavor)
    https://regexr.com/
    https://www.regexpal.com/


	* Match one character in particular (such as a):
	  "a"

	* Match one character or an other (a or b):
	  "a|b"

	* Match one character out of a set (one of a, b, c):
	  "[abcdef0123456789]"
	  "[a-f0-9]"

    * Match one or more characters out of a set (consecutive copies of a, b, c
          or d, in any order):
	  "[abcd]+"
	  "[a-d]+"

    * Match one (or more, consecutive) letter 'a's in a string
      "a+"

    * Match one or more characters out of a set (many consecutive copies of
          a, b, c or d, in any order, or nothing at all). This is called a
          "Character Class":
	  "[abcd]+"
	  "[a-d]+"

    * Match one or more characters NOT in a set (many consecutive copies of
          NOT a, b, c or d, in any order, or nothing at all). This is called a
          "Negated Character Class". Notice that the carat ^ is the 1st thing
          within the set:
	  "[^abcd]+"
	  "[^a-d]+"

    * If I want to match a string with a * in it, I must escape it:
      "\\*"


    * If I want to match a string with one or more * in it, I must escape it:
      "\\*+"

	* The dot is a character set which includes everything EXCEPT newline. This
	  pattern matches one or more chars which are not newlines, in other
	  words, an entire line of text (including a blank line):
	  ".+"

    * If I want to match a literal dot...
	  "\\."

	* Exactly match a substring:
	  "beg"

	* Match that same substring but *only* when it appears at the beginning:
	  "^beg"

	* Or match a substring *only* when it appears at the end of the line:
	  "ing$"

	* Like other programming languages you're familiar with, you can use
	  prentheses to group things together.
	  "(beginn|end)ing"


Testing out regular expressions
===============================
    There are many online resources that can help you build and debug regular expressions

    https://regex101.com/ (select the javascript flavor)
    https://regexr.com/
    https://www.regexpal.com/


<a .*href="(https*://)*([^/]+)*/*([^"]+)
