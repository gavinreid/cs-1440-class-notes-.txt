// http://www.wolframalpha.com/input/?i=volume+of+a+sphere+diameter+23
#include <iostream>
#include <cmath>
using namespace std;

int main () {
    double volume;
    double diameter;

    cout << "enter diameter of sphere ";
    cin >> diameter;

    volume = (4/3) * (3.14159265358979) * pow( diameter / 2, 3);

    cout << endl << volume << endl;
}
