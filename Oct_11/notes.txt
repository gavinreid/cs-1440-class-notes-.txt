CS 1440 - Wed Oct 11

   _                                                _
  /_\  _ _  _ _  ___ _  _ _ _  __ ___ _ __  ___ _ _| |_ ___
 / _ \| ' \| ' \/ _ \ || | ' \/ _/ -_) '  \/ -_) ' \  _(_-<
/_/ \_\_||_|_||_\___/\_,_|_||_\__\___|_|_|_\___|_||_\__/__/


Exam 2 will be at the beginning of next week, Mon Oct 16 - Weds 18

We'll have a review for Exam 2 on Friday, Oct 13

Due to Fall Break, there is no class on Thu Oct 19th

 _  ___      ___ _    _   _          _      _       
| || \ \    / / | |  | | | |_ __  __| |__ _| |_ ___ 
| __ |\ \/\/ /|_  _| | |_| | '_ \/ _` / _` |  _/ -_)
|_||_| \_/\_/   |_|   \___/| .__/\__,_\__,_|\__\___|
                           |_|                      

Look for an update to HW4 in Bitbucket soon. I'll push a new commit which will
fix a few bugs in the assignment, including

    * You don't have to calculate the number of unique and distinct wages. I
      don't know what I was thinking when I put that in the Report class...

      If you did implement those algorithms, please make a note of it in a
      README.md in your git repository and with your assignment submission in
      Canvas, and I'll instruct the graders to award you some extra credit
      points.

================================
What happens when you run out of

    * stack?
      Causes: Entering too many function calls
              Allocating too many local variables

    * heap?
      Causes: allocating too many data structures with the `new` operator
              without using `delete`.

Answer: thrashing and crashing

Thrashing:
----------
    In computer science, thrashing occurs when a computer's virtual memory
    subsystem is in a constant state of ... rapidly exchanging data in memory
    for data on disk, to the exclusion of most application-level processing.

We saw this on Cloud9 when the Memory and CPU graphs went to their limits
before the application was finally killed by the OS.

Try these for yourself on Cloud9 (or your own Linux machine) with the attached
code, or try to write your own. You'll want to be familiar with these
scenarios because they can happen in your own linked-list processing code!


====================
Debugging Strategies
====================

    Break complicated expressions up into separate lines of code
    Print out or inspect with the debugger each piece as you go

    See the code in
        sphere/
