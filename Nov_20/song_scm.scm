;;; The Scheme code in this file is compatible with Chicken Scheme version
;;; 4.11.0.
;;;
;;; It may require tweaking to run in a different implementation.

(use posix posix-extras srfi-1)


;; The recursive song that never ends
(define song
  (apply circular-list (string->list 
"This is the song that never ends
It just goes on and on my friends
Some people started singing it, not knowing what it was
And they'll continue singing it forever just because
")))

(define sing
  (lambda (s)
    (define sing-helper
      (lambda (l)
        (print* (car l)) ; The print* function is Chicken-specific.
                         ; It flushes STDOUT after each write.
        (sleep 0.001)     ; This sleep function takes a float as an argument,
                         ; and may not be portable.
        (sing-helper (cdr l))))

    (sing-helper s)))

(cond-expand
      (compiling
        (sing song))
      (else
        #t))

