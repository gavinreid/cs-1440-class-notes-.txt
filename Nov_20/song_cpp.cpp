#include <unistd.h>
#include <iostream>
#include <string>
#include <sys/types.h>

using namespace std;

struct Letter {
    char c;
    Letter* next;

    Letter(char i) : c(i), next(nullptr) {};
};

Letter* circular_list(string s) {
    Letter *head = nullptr;
    Letter *f = nullptr;

    for (char c : s) {
        if (!head) {
            head = new Letter(c);
            f = head;
        }
        else {
            f->next = new Letter(c);
            f = f->next;
        }
    }

    f->next = head;
    return head;
}

void iterative(Letter *cl) {
    while (cl) {
        cout << cl->c << flush;
        usleep(1000);
        cl = cl->next;
    }
}

void printer(char c) {
    (void)(cout << c << flush);
}

void recursive(Letter * cl) {
    usleep(1000);


    // using cout in this function causes a stack allocation
    // casting cout's return type to void didn't have an effect
    //     (void)(cout << cl->c << flush);
    // neither did putting the call to cout into its own scope.

    // To make this function tail-call optimized, comment out this line...
    cout << cl->c << flush;

    // And uncomment this line:
    // printer(cl->c);

    return recursive(cl->next);
}

std::string song = "This is the song that never ends\n"
        "It just goes on and on my friends\n"
        "Some people started singing it, not knowing what it was\n"
        "And they'll continue singing it forever just because\n";

int main(void) {
    cout << "This is the C++ version\nPID: " << getpid() << endl;

    Letter *cl = circular_list(song.c_str());

    recursive(cl);
}
