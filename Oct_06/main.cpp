#include <iostream>
#include <ostream>
using namespace std;

class Hero {
	public:
		string name;
		string role;
		Hero*  next;

	Hero(string _name, string _role, Hero* _next = (Hero*)NULL) {
		name = _name;
		role = _role;
		next = _next;
	}
};

ostream& operator<<(ostream& os, const Hero& h) {
	os << '[' << h.name << "] " << h.role << "\n";
}

void makeHeroSquad() {
	Hero *mike = new Hero("Michelangelo", "party dude");
	Hero *raph = new Hero("Raphael",      "cool but rude", mike);
	Hero *don  = new Hero("Donatello",    "does machines", raph);
	Hero *leo  = new Hero("Leonardo",     "leads", don);  

	Hero *head = leo;

	cout << *head;
}



void doThisStuff(void) {
	// Declaration:
	// allocate memory on the heap with `new`, and create
	// a pointer on the stack to point to it.
	double *pi = new double;

	// allocate an int i on the stack
	// and an int pointer j on the stack
	int i, *j; // the asterisk goes *with* the identifier, not with the type!


	// Value Assignment
	// when assigning a *value* to a pointer, you must dereference it
	// on the right-hand side of the assignment expression
	*pi = 3.14159; // ERROR!

	// you *can* assign addresses directly to a pointer
	i = 42;        // i is a value on the stack
	j = &i;        // j points to i
	j = &i;        // the variable j is on the stack, and holds i's address
	               // (i happens to also be on the stack, so its address is
				   // in the stack range of memory)


	// Dereference:
	// To use the value pointed to by the identifier pi, prefix with *
	cout << *pi << endl;

	cout << "i is the same as *j. I'll prove it to you:\n"
		"i = " << i
		<< " *j = " << *j
		<< endl;

	// If I don't dereference the pointer, instead of its value I get its memory address
	cout << "the value in `pi` lives at " << pi << endl;
	cout << "the value in `j` lives at " << j << endl;

	cout << "What does this tell us about where the stack\n"
		<< "and where the heap is located in memory?\n";
	cout << "\n\n";

	// makeHeroSquad();
}


int main() {
	// Pointer operations in C++

	// Critical Questions:

	/* What's the difference between the `heap` and the `stack`?
	 * Heap: is less organized
	 *       Stuff stays there after my functions return
	 *       On Linux and Windows is located at lower addresses
	 *
	 * Stack: more organized
	 *        Stuff "magically" disappears when my functions return :(
	 *        On Linux and Windows is located at higher addresses
	 *        Local variables and function parameters live here
	 */

	/* When do we use the stack?
	 *
	 * Whenever we declare a variable, it lives on the stack.
	 * Whenever we call a function with parameters, those parameters live on
	 * the stack.
	 *
	 * (A function's return value is placed upon the stack, too.)
	 */

	// Declaration:
	// allocate memory on the heap with `new`, and create
	// a pointer on the stack to point to it.
	int *i = new int;
	int *j = new int;

	// Value Assignment
	// when assigning a *value* to a pointer, you must dereference it
	// on the right-hand side of the assignment expression
	//
	// Try removing the * from i and j below to see how the compiler responds:
	*i = 3;
	*j = 7;

	// To use the "pointed at" value, we "dereference" it with *
	// The * symbol basically toggles our pointer into its pointed-at value
	//
	// Try removing the * from i and j to see what happens on this line of code
	cout << "i + j = " <<  *i + *j << endl;

	// The & operator returns a value's address, regardless of whether it's on
	// the stack or heap.
	// 
	// You *can* assign addresses directly to a pointer
	int k = 42;    // k is a value on the stack
	int *l = &k;   // pointer l points to k
	l = &k;        // the variable l is on the stack, and holds k's address
	               // (k happens to also be on the stack, so its address is
				   // in the stack range of memory)

	cout << "j points at address " << j << endl
		<< "l points at address " << l << endl << endl;


	// This statment demonstrates that all pointers are the same size, no
	// matter what they point to.
	//
	// This is very useful as it allows us to pass huge data structures into a
	// function without making a copy.  No matter how big the data structure
	// is, we can always pass it to a function as an 8-byte pointer.
	cout << "sizeof(int*) = " << sizeof(int*) << endl
		<< "sizeof(double*) = " << sizeof(double*) << endl;

	/* What operations use the heap?
	 * new
	 * delete
	 */

	double *pi = new double;
	delete pi;

}
