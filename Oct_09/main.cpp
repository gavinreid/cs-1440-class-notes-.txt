#include <iostream>
#include <ostream>
using namespace std;

class Hero {
	public:
		string name;
		string role;
		Hero*  next;

	Hero(string _name, string _role, Hero* _next = nullptr) {
		name = _name;
		role = _role;
		next = _next;
	}
};

ostream& operator<<(ostream& os, const Hero& h) {
	os << '[' << h.name << "] " << h.role << "\n";
	return os;
}

std::string dudes[] = {
	"Leonardo",     "leads",
	"Donatello",    "does machines",
	"Raphael",      "cool but rude",
	"Michelangelo", "party dude",
	""
};


Hero* makeHeroSquad() {
	// prime the pump
	Hero *head = new Hero(dudes[0], dudes[1]);
	Hero *temp = head;

	for (int i = 2; dudes[i] != ""; i += 2) {
		temp->next = new Hero(dudes[i], dudes[i+1]);
		temp = temp->next;
	}

	return head;
}


/*
 * Strategies for debugging:
 *
 * Using the debugger to investigate data structures
 */
int main() {
	// Make a hero squad from the back to the front
	Hero *head;
	for (head = makeHeroSquad(); head != NULL; head = head->next)
		cout << *head;
}
