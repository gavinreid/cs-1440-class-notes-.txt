CS 1440 - Fri Oct 13

Exam 2 Study Guide
 ____  ____  ____  __  ___  __ _ 
(    \(  __)/ ___)(  )/ __)(  ( \
 ) D ( ) _) \___ \ )(( (_ \/    /
(____/(____)(____/(__)\___/\_)__)
  ___  __   __ _  ____  __  ____  ____  ____   __  ____  __  __   __ _  ____ 
 / __)/  \ (  ( \/ ___)(  )(    \(  __)(  _ \ / _\(_  _)(  )/  \ (  ( \/ ___)
( (__(  O )/    /\___ \ )(  ) D ( ) _)  )   //    \ )(   )((  O )/    /\___ \
 \___)\__/ \_)__)(____/(__)(____/(____)(__\_)\_/\_/(__) (__)\__/ \_)__)(____/

We talked about the benefits of writing "modular" and "well-factored" code.
Such code exemplifies the idea of "localization of design decisions".
In short, the phrase "A place for everything, and everything in its place"
sums up these ideas.

Global variables are discouraged because their presence in a program makes it
difficult to follow the flow of data as the program evolves.

Global constants, on the other hand, are encouraged because they help us to
achieve well-factored code which exhibits "localization of design decisions".

"Magic numbers" are the opposite of global constants: as far as the CPU is
concerned they serve the same purpose as a global constant. But to a reader
lacking a deep understanding of the code base, their values seem arbitrary.
Constants with a well thought-out name explain the algorithm to the reader.

  __   ____  ____   __   _  _  ____ 
 / _\ (  _ \(  _ \ / _\ ( \/ )/ ___)
/    \ )   / )   //    \ )  / \___ \
\_/\_/(__\_)(__\_)\_/\_/(__/  (____/

An array is a fixed data structure. Its size cannot be changed after it has
been created. An array does not need to be sorted to find the minimum or
maximum values. In any case, the entire array must be traversed to find such a
value. Why?

Sorting an array may be advantageous to finding other interesting values and
properties of the dataset: median, mode, number of distinct or unique values,
the minimum and maximum N values.

A sorting algorithm may be stable or unstable.
A stable sorting algorithm preserves the original relative order between two
elements in the case of a tie on its sort key.

It is profitable to restate a problem into your own words as it makes clear to
you the patterns in the problem. You may also discover that some details are
irrelevant to the problem, further reducing the amount of work needed to
arrive at a solution.

Dividing the problem into compartments serves N purposes:
    0. Breaks the problems into manageable pieces (how do you eat an elephant?)
    1. Allows you to focus on those aspects in which you are confident (start
       with what you know)
    2. Understand how the components of your solution can work together
    3. Identify the pieces which you don't yet know how to solve; focus your
       efforts here on experimentation

To divide a problem you may use these techniques:
    0. draw a diagram of the problem
    1. identify "nouns" and "verbs" in the problem

 ____   __  __  __ _  ____  ____  ____  ____ 
(  _ \ /  \(  )(  ( \(_  _)(  __)(  _ \/ ___)
 ) __/(  O ))( /    /  )(   ) _)  )   /\___ \
(__)   \__/(__)\_)__) (__) (____)(__\_)(____/

A pointer is a variable which holds the address to another value. Despite
being confusing to programmers, they are a very natural concept to your CPU.
They underpin all programming languages (even those who hide their details
from the programmer).

In C++, we use the * symbol when we
    0. declare pointer variables
        double *d;
    1. use the pointed-at value
        *d = 3.14159;
        cout << *d << endl;

The * goes with the name of the variable in a declaration. In the following
compound declaration, only p is a pointer:
    int* p, i, j;

In C++, we use the & symbol when we want to discover the address of a
variable. The address of a variable may be assigned to a pointer:
    double d = 3.14159;
    double *ptr0 = &d;

When we assign an address to a pointer, we don't use the * in the assignment:
    double *ptr1;
    ptr1 = &d;

We only use the * in a pointer declaration, or in an expression where we want
to refer to the pointed-at value. Following the pointer to the value it points
to is called "dereferencing".


 ____  _  _  __ _   __   _  _  __  ___    ____   __  ____  __  
(    \( \/ )(  ( \ / _\ ( \/ )(  )/ __)  (    \ / _\(_  _)/ _\ 
 ) D ( )  / /    //    \/ \/ \ )(( (__    ) D (/    \ )( /    \
(____/(__/  \_)__)\_/\_/\_)(_/(__)\___)  (____/\_/\_/(__)\_/\_/
 ____  ____  ____  _  _   ___  ____  _  _  ____  ____  ____ 
/ ___)(_  _)(  _ \/ )( \ / __)(_  _)/ )( \(  _ \(  __)/ ___)
\___ \  )(   )   /) \/ (( (__   )(  ) \/ ( )   / ) _) \___ \
(____/ (__) (__\_)\____/ \___) (__) \____/(__\_)(____)(____/

Dynamic data structures make use of the advantages pointers bring us,
allowing, among other benefits, being resizable at runtime.

Linked lists are an example of a dynamic data structure. The list can be used
similarly to a sentinel-delimited array, despite being quite different in
implementation.

Like an array, a linked list may be
    * traversed from front to back
    * searched 
    * sorted

Unlike an array, an element of a linked list can't be easily or quickly
accessed. You must follow all of the next pointers from the head to the
element you want.

 _  _  ____   __   ____    _  _  ____       ____  ____  __    ___  __ _ 
/ )( \(  __) / _\ (  _ \  / )( \/ ___)     / ___)(_  _)/ _\  / __)(  / )
) __ ( ) _) /    \ ) __/  \ \/ /\___ \ _   \___ \  )( /    \( (__  )  ( 
\_)(_/(____)\_/\_/(__)     \__/ (____/(_)  (____/ (__)\_/\_/ \___)(__\_)

Dynamic data structures are usually allocated in a region of memory known as
the heap. Ordinary variables, by contrast, are allocated in a region of memory
called the stack.

What's the difference between the `heap` and the `stack`?
    Heap: is less organized
        Stuff stays there after my functions return
        On Linux and Windows is located at lower addresses
     
     Stack: more organized
         Stuff "magically" disappears when my functions return :(
         On Linux and Windows is located at higher addresses
         Local variables and function parameters live here

When do we use the heap?
    In C++, whenever we use the `new` operator we're allocating memory on the
    heap.

When do we use the stack?
Whenever we declare a variable, it is allocated on the stack:
    int i, *p; 
    i = 1337;
    p = new int(42);

i is an integer allocated on the stack.

p is a pointer allocated on the stack. The integer value 42 was allocated on
the heap. The variable p contains an address to that 42 out on the heap. But
the address, which is stored in the variable p, is stored on the stack.

Whenever we call a function with parameters, the parameters are allocated on
the stack. A function's return value is placed upon the stack, too.  The stack
makes aspects of memory allocation automatic; variables are allocated for us
upon entry to the function, and are deallocated for us as we leave. This
concept is called the "lifetime" of a variable.

The heap gives us a place to store values which we do not wish to be
deallocated when a function returns. Doing so allows us to work around the
limitation of functions returning only a single value.

There are a few well-known bugs related to the heap.

Heap overflows occur when we run out of heap memory.

We may lose track of the addresses of our allocated chunks of heap memory -
this is called a memory leak.

We might be diligent about deallocating unused heap memory so as to avoid
leaking memory. But if a pointer variable which still retains an address which
has been deallocated, we've created a dangling pointer.
