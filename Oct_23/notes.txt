CS 1440 - Mon Oct 23

______ _____ _     _____                       _   _             
|  ___/  ___| |   /  __ \                     | | (_)            
| |_  \ `--.| |   | /  \/  _ __ ___   ___  ___| |_ _ _ __   __ _ 
|  _|  `--. \ |   | |     | '_ ` _ \ / _ \/ _ \ __| | '_ \ / _` |
| |   /\__/ / |___| \__/\ | | | | | |  __/  __/ |_| | | | | (_| |
\_|   \____/\_____/\____/ |_| |_| |_|\___|\___|\__|_|_| |_|\__, |
                    Thursday Oct 26 - 7:00 pm               __/ |
                           ENGR 203                        |___/ 

Installing a collection of Linux distros on a bunch of computers
to create an FSLC display for Science Unwrapped.



 ___                  ___   ___         _            
| __|_ ____ _ _ __   |_  ) | _ \_____ _(_)_____ __ __
| _|\ \ / _` | '  \   / /  |   / -_) V / / -_) V  V /
|___/_\_\__,_|_|_|_| /___| |_|_\___|\_/|_\___|\_/\_/ 
                                                     
(86%)
Q: Why are global constants encouraged?
A: They increase readability and represent a single source of truth for a particular value


(83%)
Q: What's undesirable about "magic numbers"?
A: Their meaning can be difficult to understand


(69%)
Q: What is meant by "localization of design decicions"?
A: A place for everything, and everything in its place


(67%)
Q: Why is modular code desirable?
A: Reduces code duplication
A: Flow of data through program is easier to follow


(56%) /* regraded */
Q: What are some techniques you can use to divide the problem?
A: Identify "nouns" and "verbs" in the problem statement
   Draw a diagram of the problem


(0%) - I goofed up on this one, so I gave all of you credit.
Q: Which are advantages of dynamic data structures?
A: Can be used in situations where the maximum size cannot be determined at compile-time
A: The data structure can grow or shrink as appropriate


(78%)
Q: Why does the compiler keep track of the type of the value at the other end
   of the pointer?
A: Otherwise, how would your program interpret the 1's and 0's located at the
   pointed-to address?


(68%) /* regraded */
Q: Which bug does this code exhibit?
   
   unsigned *u = NULL;
   for (int i = 0; i >= 0; i++) {
        u = new unsigned[16384];
		u = NULL;
   }
   
A: Heap overflow.


(61%)
Q: This is a valid sequence of C++ statements:
   
   int *i;
   i = &42;
   
A: False


(71%) /* regraded */
Q: This is a valid sequence of C++ statements:
   
   double *e /* = new double */ ;
   *e = 2.71828;

A: True


  ___ _    ___             _    
 / __| |_ | __|  __ _ _  _(_)___
| (__| ' \|__ \ / _` | || | |_ /
 \___|_||_|___/ \__, |\_,_|_/__|
                   |_|          
Close your books and get out your pencils

  ___ _    ___      _ _                   _          
 / __| |_ | __|  __| (_)___ __ _  _ _____(_)___ _ _  
| (__| ' \|__ \ / _` | (_-</ _| || (_-<_-< / _ \ ' \ 
 \___|_||_|___/ \__,_|_/__/\__|\_,_/__/__/_\___/_||_|
                                                     
	Visit with your neighbors and brainstorm ways you might apply the lessons
	of Chapter 5 to your current homework assignment.

	Answer these questions:
	-----------------------

	In what ways could you apply the lessons of Chapter 5 to HW4/5?

	How might the concepts of Encapsulation and Information Hiding be
	leveraged to improve the design of your program?

	What "support methods" exist in your current design?

	What would it take to convert them from ordinary functions to class
	methods?

	What other "support methods" could you imagine for your Linked List?
