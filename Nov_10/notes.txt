CS 1440 - Fri Nov 10

 ___                    _ _   ____
| __|_ ____ _ _ __    _| | |_|__ / Tuesday Nov 14th - Thursday Nov 16th
| _|\ \ / _` | '  \  |_  .  _||_ \ (The funny schedule is to avoid
|___/_\_\__,_|_|_|_| |_     _|___/  the Hackathon Nov 17th-18th)
                       |_|_|       Sign up at the Testing Center today!

Exam #3 will consist of material covered between Oct 16 through November 10

-=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=-

 _  _  
| || |ow to begin when you have writer's block
| __ |
|_||_|

To get started you will want to read through the .hpp files and create a UML
class diagram that captures the classes in the starter code.

Then carefully read through the overview and the requirements and
decide

    0) which new collections of data will the solution need?
	1) which new behaviors will the solution need to exhibit?

Once you come up with this list you should decide where those data and
behaviors need to live.

	* Do some of them belong in the classes already provided?

	* Which new classes might you create that would be a better home for those
	  data/behaviors?

	* How will those new classes relate to the starting code?

When figuring out how the new classes fit together, you might find it helpful
to print a few copies of your original UML diagram and sketch right on top of
that with a pencil your new ideas. Once you're satisfied with that design,
draw it up on the computer.

