#include <iostream>
using namespace std;

int prepared_distinct(int array[], int len) {
	int count = 0,
		prev = -1,
		curr;

	for (int i = 0; i < len; ++i) {
		curr = array[i];
		if (prev != curr) {
			count++;
		}
		prev = curr;
	}

	return count;
}

int our_distinct(int array[], int len) {
    int count = 1;

    if (len < 1)
        return 0;

    for (int i = 1; i < len; ++i) {
        if (array[i] != array[i-1]) {
            count++;
        }
    }

    return count;
}

int prepared_unique(int array[], int len) {
	int count = 0,
		seen = 0,
		prev = -1,
		curr;

	for (int i = 0; i < len; ++i) {
		curr = array[i];
		if (prev != curr) {
			if (seen == 1)
				count++;
			seen = 1;
			prev = curr;
		}
		else {
			seen++;
		}
	}
	if (seen == 1) {
		count++;
	}

	return count;
}

int our_unique(int array[], int len) {
    if (len < 1)
        return 0;
    else if (len == 1)
        return 1;

    int count = 0,
        seen = 0;

    for (int i = 1; i < len; ++i) {
        if (array[i] == array[i-1]) {
            seen++;
        }
        else {
            if (seen == 0) {
                count++;
            }
            seen = 0;
        }
    }

    return count;
}

int numbers[] = {1, 1, 2, 3, 4, 4, 4, 5, 6, 6 };
const int numbers_l = sizeof(numbers) / sizeof(int);

int main(void) {

	cout << "This is the list:\nnumbers = { ";
	for (int i = 0; i < numbers_l; ++i)
		cout << numbers[i] << ", ";
	cout << "\b\b }\n";

	cout << "there are " << prepared_unique(numbers, numbers_l)
		<< " unique items in this list\n";

	cout << "our new function found " << our_unique(numbers, numbers_l)
		<< " unique items in this list\n";

	cout << "there are " << prepared_distinct(numbers, numbers_l)
		<< " distinct items in this list\n";

	cout << "our new function found " << our_distinct(numbers, numbers_l)
		<< " distinct items in this list\n";
}
